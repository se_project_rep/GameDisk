import java.io.*;
import java.sql.*;

public class DBConnection {
    private Connection conn = null;
    private Statement statement = null;
    private PreparedStatement stmt = null;
    private ResultSet resultSet = null; 
    
    private final String username = "root";
    private final String password = "";
    private final String host = "jdbc:mysql://localhost:3306/gamedisk?useSSL=false";
        
    public void setConn(){
        try{
            conn = DriverManager.getConnection(host, username, password);
            System.out.println("Database connected!");
        }
        catch (SQLException e){
            System.err.print(e);
        }
    }
    
    public void dbClose(){
        if (conn != null) {
            try {
                conn.close();
                System.out.println("Database is closed.");
            }
            catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    public void setStatement(String query) throws SQLException{
        this.statement = conn.createStatement();
        this.statement.execute(query);
    }

    public void setRS(String query) throws SQLException{
        this.statement = conn.createStatement();
        this.statement.executeQuery(query);
        this.resultSet = this.statement.getResultSet();
    }
    public ResultSet getRS() throws SQLException{
        return this.resultSet;
    }
    public void setBiStream(FileInputStream input, String query) throws SQLException{
        this.stmt = this.conn.prepareStatement(query);
        this.stmt.setBinaryStream(1, input);
        this.stmt.executeUpdate();
    }
}
