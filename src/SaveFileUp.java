import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.zip.*;

public class SaveFileUp {
    DBConnection dbc = new DBConnection();
    
    private File file;
    private FileInputStream input = null;
    private String dirr = "";
    private String gname;
    
    private int admin_id;
    private int client_id;
    
    public SaveFileUp(){
        
    }
    
    public void getID(int admin_id, int client_id){
        this.admin_id = admin_id;
        this.client_id = client_id;
    }
    
    public void setFile(String gname) throws SQLException{
        String query = "SELECT * FROM `admin_log` WHERE admin_id = '"+this.admin_id+"'";
      
        dbc.setConn();
        dbc.setRS(query);
        System.out.println("View Success!");
            
        while(dbc.getRS().next()){
            if(dbc.getRS().getString("filename").equals(gname)){
                this.dirr = dbc.getRS().getString("save_path");
            }
        }
        this.dirr += ".zip";
        this.gname = gname;
        System.out.println(this.dirr);
        dbc.dbClose();
    }
    
    public void getUpFile() throws SQLException, FileNotFoundException{
        this.file = new File(this.dirr);
        this.input = new FileInputStream(this.file);
        
        String query = "SELECT * FROM `save` WHERE client_id = '"+this.client_id+"' AND game_name = '"+this.gname+"'";
        String query1 = "SELECT * FROM `save` WHERE `client_id`='"+this.client_id+"'";
        
        dbc.setConn();
        dbc.setRS(query1);
        
        if(dbc.getRS().next()){
            dbc.setConn();
            dbc.setRS(query);
            if(dbc.getRS().next()){
                System.out.println("Failed to Input1!");
                this.setBlob(this.input);
            }
            else{
                this.firstUpload(this.gname);
                this.setBlob(this.input);
            }
        }
        else{
            this.firstUpload(this.gname);
            this.setBlob(this.input);
        }
        
           
    }
    
    private void firstUpload(String gname) throws SQLException{
        String query = "INSERT INTO `save`(`client_id`, `game_name`) VALUES"
                + "('"+this.client_id+"','"+gname+"')";
        
        dbc.setConn();
        dbc.setStatement(query);
        System.out.println("Input1 Success!");
    }
    private void setBlob(FileInputStream input) throws SQLException{
        String query = "UPDATE `save` SET `file`=? WHERE `game_name`='"+this.gname+"'";
        dbc.setConn();
        dbc.setBiStream(input,query);
        System.out.println("Input2 Success!");
    }
}
