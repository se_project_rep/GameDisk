import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.zip.*;

public class SaveFile {
    DBConnection dbc = new DBConnection();
    private ArrayList<String> fileList;
    private String OUTPUT_ZIP_FILE;
    private String SOURCE_FOLDER;
    private String name = "";
    
    private int admin_id;
    private int client_id;
    
    public SaveFile(){
    }
    
    public void SaveFile(String name) throws SQLException{
        fileList = new ArrayList<String>();
        this.name = this.getName(name);
        String dirr = this.getDirr();
        SOURCE_FOLDER = dirr;
        OUTPUT_ZIP_FILE = this.SOURCE_FOLDER+".zip";
    	this.generateFileList(new File(SOURCE_FOLDER));
    	this.zipIt(OUTPUT_ZIP_FILE);
    }
    
    
    private void zipIt(String zipFile){
        byte[] buffer = new byte[1024];
        try(
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos))
            {
            System.out.println("Output to Zip : " + zipFile);

            for(String file : this.fileList){
                System.out.println("File Added : " + file);
                ZipEntry ze  = new ZipEntry(file);
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            //remember close it

            System.out.println("Done");
        }catch(IOException ex){
           System.out.println(ex);
        }
    }
    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     * @param node file or directory
     */
    private void generateFileList(File node){
    	//add file only
	if(node.isFile()){
		fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
	}
	if(node.isDirectory()){
            String[] subNote = node.list();
            for(String filename : subNote){
		generateFileList(new File(node, filename));
            }
	}
    }
    /**
     * Format the file path for zip
     * @param file path
     * @return Formatted file path
     */
    private String generateZipEntry(String file){
    	return file.substring(SOURCE_FOLDER.length()+1, file.length());
    }
    
    private String getName(String name){
        this.name = name;
        return this.name;
    }
    private String getDirr() throws SQLException{
        String query = "SELECT * FROM `admin_log` WHERE admin_id = '"+this.admin_id+"'";
        String namef = "";
      
        dbc.setConn();
        dbc.setRS(query);
        System.out.println("View Success!");
            
        while(dbc.getRS().next()){
            if(dbc.getRS().getString("filename").equals(this.name)){
                namef = dbc.getRS().getString("save_path");
                
            }
        }
        dbc.dbClose();
        
        return namef;
    }
    public void getID(int admin_id, int client_id){
        this.admin_id = admin_id;
        this.client_id = client_id;
    }
}
