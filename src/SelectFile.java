/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Root
 */
import java.util.*;
import java.io.*;
import javax.swing.JFileChooser;
import java.sql.*;
import javax.swing.JOptionPane;

public class SelectFile {
    DBConnection dbc = new DBConnection();
    private JFileChooser chooser;
    
    private String sourceFolder = "";
    private String source = "";
    private String theFile = "";
    private String dr = "";
    private String dirr;
    private String sPath = "";
    private String s_path = "";
    private File file;
    
    private int admin_id;
    private int client_id;
    
    public SelectFile(){
    }
    
    public void openFile(){
        chooser = new JFileChooser();
        char[] ch = new char[100];
        if(chooser.showOpenDialog(chooser) == JFileChooser.APPROVE_OPTION){
            this.dirr = "" + chooser.getCurrentDirectory();
            this.file = chooser.getSelectedFile();
            
            for(int x=0; x<this.dirr.length(); x++){
                ch[x] = this.dirr.charAt(x);
                if(ch[x] == '\\'){
                    ch[x] = '/';
                }
                this.dr += ch[x];
            }
            
            this.sourceFolder = "" + this.dr + "\\" + this.file.getName();
            for(int x=0; x<this.sourceFolder.length(); x++){
                ch[x] = this.sourceFolder.charAt(x);
                if(ch[x] == '\\'){
                    ch[x] = '/';
                }
                this.source += ch[x];
            }
            
            System.out.println("Folder path: " + this.dr);
            System.out.println("File name: " + this.file.getName());
            System.out.println(this.source);
            this.theFile = this.file.getName();
            
            this.setAddfile(this.dr, this.theFile, this.source);
            this.dr = "";
            this.theFile = "";
            this.source = "";
        }
        
        
    }
    public String getfileName(){
        return this.file.getName();
    }
    private void setAddfile(String dirr, String fileName, String source){
        String query = "INSERT INTO admin_log(`admin_id`,`dirr`, `filename`, `source`) VALUES"
                + "('"+this.admin_id+"', '"+dirr+"', '"+fileName+"', '"+source+"')";
        
        try{
            dbc.setConn();
            dbc.setStatement(query);
            System.out.println("Input Success!");
        }
        catch(SQLException e){
            System.err.println(e);
            System.out.println("SelectFile(setAddfile err)");
        }
        dbc.dbClose();
    }
    public void setPath(String path, int index) throws SQLException{
        chooser = new JFileChooser();
        char[] ch = new char[100];
        this.chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if(chooser.showOpenDialog(chooser) == JFileChooser.APPROVE_OPTION){
            this.file = chooser.getSelectedFile();
            this.sPath = this.file.toString();
            
            for(int x=0; x<this.sPath.length(); x++){
                ch[x] = this.sPath.charAt(x);
                if(ch[x] == '\\'){
                    ch[x] = '/';
                }
                this.s_path += ch[x];
            }
            System.out.println("Path: "+this.s_path);
            System.out.println("Selected.");
            this.setUpdatePath(this.s_path, index);
            this.s_path = "";
        }
        else{
            System.out.println("Cancelled.");
        }
    }
    private void setUpdatePath(String path, int index) throws SQLException{
        String query = "UPDATE `admin_log` SET `save_path`='"+path+"' WHERE `log_id`='"+(index+1)+"'";
        dbc.setConn();
        dbc.setStatement(query);
        System.out.println("Update Success!");
        dbc.dbClose();
    }
    public String getPath(String name) throws SQLException{
        String query = "SELECT * FROM `admin_log` WHERE admin_id = '"+this.admin_id+"'";
        String pname = "";
        
        dbc.setConn();
        dbc.setRS(query);
        while(dbc.getRS().next()){
            if(dbc.getRS().getString("filename").equals(name)){
                pname = dbc.getRS().getString("save_path");
            }
        }
        
        return pname;
    }
    public void getID(int admin_id, int client_id){
        this.admin_id = admin_id;
        this.client_id = client_id;
    }
}
